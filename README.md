An ERC-20-like token smart contract for the Substrate Contracts palette.

Allows instantiating fungible tokens with different configuration options.

# Features

* Mintable and non-mintable tokens
* Burnable and non-burnable tokens
* Token transfers with lockout periods

## Mintability

When creating a token, the token can be chosen to be mintable or non-mintable. If a token is mintable, then the token authority (the account that created the token) can increase the total supply of tokens by "minting" more of them. If a token is non-mintable, then the total supply is limited at creation time, and can never be increased.

## Burnability

When creating a token, the token can be chosen to be:
1. Burnable by token authority, meaning tokens can be burned from any account by the token authority,
2. Burnable by token holders, meaning tokens can be burned by their owners.

Burned tokens are removed from total supply.

## Lockout

When transferring tokens from one account to another, an optional lockout period can be chosen. The recepient will not be able to use the tokens until the lockout period has expired.

Locked tokens cannot be burned until the lockout period has expired.

# API

## `Token`

```rust

/// An ERC-20-like token smart contract.
#[ink(storage)]
pub struct Token {
    // ... omittted
}

/// Emitted when funds have been transferred to/from an account.
///
/// This includes minting/burning, in which case either the `from` or the `to` topic will be absent.
#[ink(event)]
pub struct TransferEvent {
    /// Account which sent the funds.
    ///
    /// Will be `None` if tokens were minted.
    #[ink(topic)]
    pub from: Option<AccountId>,
    /// Account which received the funds.
    ///
    /// Will be `None` if tokens were burned.
    #[ink(topic)]
    pub to: Option<AccountId>,

    /// Amount of tokens transferred.
    pub amount: u128,

    /// Timestamp when the funds will be unlocked, if the transfer was locked.
    pub unlock_timestamp: Option<u64>,
}

/// Emitted when funds were unlocked for an account.
///
/// Can happen automatically in [`Token::transfer`] or [`Token::transfer_locked`], or manually in [`Token::try_unlock`].
#[ink(event)]
pub struct UnlockEvent {
    /// Wallet for which funds were unlocked.
    #[ink(topic)]
    pub wallet: AccountId,

    /// Amount of unlocked funds.
    pub amount: u128,
    /// Timestamp of unlocked funds. This is not necessarily the current time, but rather the scheduled time that was set during the transfer.
    pub unlock_timestamp: u64,
}

impl Token {
    /// Creates a new mintable token.
    ///
    /// A mintable token can increase its total supply by "minting" new tokens, which is an operation
    /// permitted only for the token authority.
    ///
    /// Additional parameters can be set via the `config` argument.
    #[ink(constructor)]
    pub fn new_mintable(config: InstantiationConfig) -> Self;

    /// Creates a token with a limited initial supply. Its total supply can never be increased
    /// after instantiation. Initial supply is deposited to the account that created this token.
    ///
    /// Additional parameters can be set via the `config` argument.
    ///
    /// If the token is also burnable (either by token authority or holders), then its supply can decrease.
    ///
    /// If the token is not burnable, the supply is fixed forever.
    #[ink(constructor)]
    pub fn new_with_limited_supply(initial_supply: u128, config: InstantiationConfig) -> Self;

    /// Return the free balance of an account.
    #[ink(message)]
    pub fn free_balance_of(&self, of: AccountId) -> u128;

    /// Return the total balance of an account, including locked tokens.
    #[ink(message)]
    pub fn total_balance_of(&self, of: AccountId) -> u128;

    /// Returns the total supply of tokens.
    #[ink(message)]
    pub fn total_supply(&self) -> u128;

    /// Returns the total free supply of tokens.
    #[ink(message)]
    pub fn total_free_supply(&self) -> u128;

    /// Returns the total *locked* supply of tokens.
    #[ink(message)]
    pub fn total_locked_supply(&self) -> u128;

    /// Mint new tokens for an account.
    ///
    /// This will fail if the caller is not the token authority, or if the token is not mintable.
    #[ink(message)]
    pub fn mint(&mut self, target: AccountId, amount: u128) -> Result<(), Error>;

    /// Burn tokens from an account.
    ///
    /// This will only work if the token is either:
    /// 1. Burnable by token holder, and the caller is the owner of the target account,
    /// 2. Burnable by token authority, and the caller is the token authority.
    #[ink(message)]
    pub fn burn(&mut self, target: AccountId, amount: u128) -> Result<(), Error>;

    /// Transfer tokens from one account to another.
    ///
    /// If necessary, will check if any tokens can be unlocked at the current time, and unlock them.
    #[ink(message)]
    pub fn transfer(&mut self, destination: AccountId, amount: u128) -> Result<(), Error>;

    /// Transfer token from one account to another, but lock them to only be released at a specific time.
    ///
    /// If necessary, will check if any tokens can be unlocked at the current time, and unlock them.
    ///
    /// The recepieint will only be able to use the tokens after the specified timestamps have been reached.
    #[ink(message)]
    pub fn transfer_locked(&mut self, destination: AccountId, amounts: Vec<(u64, u128)>) -> Result<(), Error>;

    /// Tries to unlock tokens from the target account, if any can be unlocked at the current time.
    #[ink(message)]
    pub fn try_unlock(&mut self, target: AccountId) -> Result<(), Error>;
}

```
