#![cfg_attr(not(feature = "std"), no_std)]

use ink_lang as ink;
use ink_prelude::string::String;
use ink_prelude::vec::Vec;
use ink_storage::traits::{PackedLayout, SpreadLayout};

#[cfg(feature = "std")]
use ink_storage::traits::StorageLayout;

use scale::{Decode, Encode};

#[derive(Default, Eq, PartialEq, Debug, Encode, Decode, SpreadLayout, PackedLayout)]
#[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
struct Wallet {
    free_balance: u128,
    total_balance: u128,
    locked_qtys: Vec<(u64, u128)>,
}

impl Wallet {
    fn add_free(&mut self, amount: u128) -> Result<(), Error> {
        self.free_balance = self
            .free_balance
            .checked_add(amount)
            .ok_or(Error::ArithmeticOverflow)?;

        self.total_balance = self
            .total_balance
            .checked_add(amount)
            .ok_or(Error::ArithmeticOverflow)?;

        Ok(())
    }

    fn remove_free(&mut self, amount: u128) -> Result<(), Error> {
        if self.free_balance < amount {
            return Err(Error::InsufficientFunds);
        }

        self.free_balance -= amount;
        self.total_balance -= amount;

        Ok(())
    }

    fn add_locked(&mut self, unlock_timestamp: u64, amount: u128) -> Result<(), Error> {
        let position = self
            .locked_qtys
            .iter()
            .enumerate()
            .find(|(_, (timestamp, _))| *timestamp > unlock_timestamp)
            .map(|(i, _)| i)
            .unwrap_or(self.locked_qtys.len());

        self.locked_qtys
            .insert(position, (unlock_timestamp, amount));

        self.total_balance = self
            .total_balance
            .checked_add(amount)
            .ok_or(Error::ArithmeticOverflow)?;

        Ok(())
    }

    fn unlock_at(&mut self, timestamp: u64) -> Result<(), Error> {
        if self.locked_qtys.is_empty() || self.locked_qtys[0].0 > timestamp {
            return Ok(());
        }

        let mut to_remove = 0;
        for (unlock_timestamp, amount) in &self.locked_qtys {
            if *unlock_timestamp <= timestamp {
                to_remove += 1;
                self.free_balance += *amount;
            } else {
                break;
            }
        }

        self.locked_qtys.drain(..to_remove);

        Ok(())
    }
}

#[derive(Default, Eq, PartialEq, Debug, Encode, Decode, SpreadLayout, PackedLayout)]
#[cfg_attr(feature = "std", derive(scale_info::TypeInfo, StorageLayout))]
/// Supplemental information about a token.
struct TokenInfo {
    name: String,
    tag: String,
    decimals: u8,
}

#[derive(Default, Eq, PartialEq, Debug, Encode, Decode, SpreadLayout, PackedLayout)]
#[cfg_attr(feature = "std", derive(scale_info::TypeInfo, StorageLayout))]
/// Configuration parameters of a token instance.
struct TokenConfig {
    mintable: bool,
    burnable_by_token_authority: bool,
    burnable_by_holder: bool,
}

#[derive(Default, Eq, PartialEq, Debug, Encode, Decode, SpreadLayout, PackedLayout)]
#[cfg_attr(feature = "std", derive(scale_info::TypeInfo, StorageLayout))]
/// Parameters that can be set for a token at instantiation time.
pub struct InstantiationConfig {
    /// Token's full display name.
    ///
    /// E.g. Bitcoin
    pub name: String,
    /// Token's shortened tag.
    ///
    /// E.g. BTC
    pub tag: String,
    /// Number of decimal places of this token in decimal representation.
    ///
    /// E.g. 8 for Bitcoin. 100000000 units is 1 BTC, 1 unit is 1 Satoshi.
    pub decimals: u8,

    /// Whether this token can be burned by the token authority.
    pub burnable_by_token_authority: bool,
    /// Whether this token can be burned by token holders.
    pub burnable_by_holder: bool,
}

#[derive(Debug, Encode, Decode)]
#[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
pub enum Error {
    NotAuthorized,
    ArithmeticOverflow,
    InsufficientFunds,
    NotMintable,
    NotBurnable,
}

#[ink::contract]
pub mod token {
    use super::*;
    use ink_prelude::*;
    use ink_storage::{collections::HashMap, Lazy};

    /// An ERC-20-like token smart contract.
    ///
    /// Features:
    /// * Mintable and non-mintable tokens
    /// * Burnable and non-burnable tokens
    /// * Token transfers with lockout periods
    ///
    /// See constructors and methods for more information.
    #[ink(storage)]
    pub struct Token {
        info: Lazy<TokenInfo>,
        config: Lazy<TokenConfig>,
        supply: Lazy<u128>,
        authority: Lazy<AccountId>,
        wallets: HashMap<AccountId, Wallet>,
    }

    /// Emitted when funds have been transferred to/from an account.
    ///
    /// This includes minting/burning, in which case either the `from` or the `to` topic will be absent.
    #[ink(event)]
    pub struct TransferEvent {
        #[ink(topic)]
        /// Account which sent the funds.
        ///
        /// Will be `None` if tokens were minted.
        pub from: Option<AccountId>,
        /// Account which received the funds.
        ///
        /// Will be `None` if tokens were burned.
        #[ink(topic)]
        pub to: Option<AccountId>,

        /// Amount of tokens transferred.
        pub amount: u128,

        /// Timestamp when the funds will be unlocked, if the transfer was locked.
        pub unlock_timestamp: Option<u64>,
    }

    /// Emitted when funds were unlocked for an account.
    ///
    /// Can happen automatically in [`Token::transfer`] or [`Token::transfer_locked`], or manually in [`Token::try_unlock`].
    #[ink(event)]
    pub struct UnlockEvent {
        /// Wallet for which funds were unlocked.
        #[ink(topic)]
        pub wallet: AccountId,

        /// Amount of unlocked funds.
        pub amount: u128,
        /// Timestamp of unlocked funds. This is not necessarily the current time, but rather the scheduled time that was set during the transfer.
        pub unlock_timestamp: u64,
    }

    impl Token {
        /// Creates a new mintable token.
        ///
        /// A mintable token can increase its total supply by "minting" new tokens, which is an operation
        /// permitted only for the token authority.
        ///
        /// Additional parameters can be set via the `config` argument.
        #[ink(constructor)]
        pub fn new_mintable(config: InstantiationConfig) -> Self {
            let authority = Self::env().caller();

            let info = TokenInfo {
                name: config.name,
                tag: config.tag,
                decimals: config.decimals,
            };

            let config = TokenConfig {
                mintable: true,
                burnable_by_token_authority: config.burnable_by_token_authority,
                burnable_by_holder: config.burnable_by_holder,
            };

            Self {
                info: Lazy::new(info),
                config: Lazy::new(config),
                supply: Lazy::new(0),
                authority: Lazy::new(authority),
                wallets: HashMap::new(),
            }
        }

        /// Creates a token with a limited initial supply. Its total supply can never be increased
        /// after instantiation. Initial supply is deposited to the account that created this token.
        ///
        /// Additional parameters can be set via the `config` argument.
        ///
        /// If the token is also burnable (either by token authority or holders), then its supply can decrease.
        ///
        /// If the token is not burnable, the supply is fixed forever.
        #[ink(constructor)]
        pub fn new_with_limited_supply(initial_supply: u128, config: InstantiationConfig) -> Self {
            let authority = Self::env().caller();

            let info = TokenInfo {
                name: config.name,
                tag: config.tag,
                decimals: config.decimals,
            };

            let config = TokenConfig {
                mintable: true,
                burnable_by_token_authority: config.burnable_by_token_authority,
                burnable_by_holder: config.burnable_by_holder,
            };

            let mut token = Self {
                info: Lazy::new(info),
                config: Lazy::new(config),
                supply: Lazy::new(0),
                authority: Lazy::new(authority),
                wallets: HashMap::new(),
            };

            token
                .mint(authority, initial_supply)
                .expect("initial supply deposit must not fail");

            token.config.mintable = false;

            token
        }

        fn wallet_mut(&mut self, account: AccountId) -> &mut Wallet {
            self.wallets.entry(account).or_insert_with(Default::default)
        }

        /// Return the free balance of an account.
        #[ink(message)]
        pub fn free_balance_of(&self, of: AccountId) -> u128 {
            self.wallets
                .get(&of)
                .map(|wallet| wallet.free_balance)
                .unwrap_or(0)
        }

        /// Return the total balance of an account, including locked tokens.
        #[ink(message)]
        pub fn total_balance_of(&self, of: AccountId) -> u128 {
            self.wallets
                .get(&of)
                .map(|wallet| wallet.total_balance)
                .unwrap_or(0)
        }

        /// Returns the total supply of tokens.
        #[ink(message)]
        pub fn total_supply(&self) -> u128 {
            *self.supply
        }

        /// Returns the total free supply of tokens.
        #[ink(message)]
        pub fn total_free_supply(&self) -> u128 {
            let mut amount = 0;
            for (_, wallet) in &self.wallets {
                amount += wallet.free_balance;
            }

            amount
        }

        /// Returns the total *locked* supply of tokens.
        #[ink(message)]
        pub fn total_locked_supply(&self) -> u128 {
            self.total_supply() - self.total_free_supply()
        }

        /// Mint new tokens for an account.
        ///
        /// This will fail if the caller is not the token authority, or if the token is not mintable.
        #[ink(message)]
        pub fn mint(&mut self, target: AccountId, amount: u128) -> Result<(), Error> {
            if !self.config.mintable {
                return Err(Error::NotMintable);
            }

            let authority = *self.authority;
            if authority != self.env().caller() {
                return Err(Error::NotAuthorized);
            }

            let wallet = self.wallet_mut(target);
            wallet.add_free(amount)?;

            *self.supply = self
                .supply
                .checked_add(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            Ok(())
        }

        /// Burn tokens from an account.
        ///
        /// This will only work if the token is either:
        /// 1. Burnable by token holder, and the caller is the owner of the target account,
        /// 2. Burnable by token authority, and the caller is the token authority.
        #[ink(message)]
        pub fn burn(&mut self, target: AccountId, amount: u128) -> Result<(), Error> {
            let authority = *self.authority;
            let config = self.config.as_ref();

            let mut allowed = false;
            if authority == self.env().caller() && config.burnable_by_token_authority {
                allowed = true
            }

            if self.env().caller() == target && config.burnable_by_holder {
                allowed = true
            }

            if !allowed {
                return Err(Error::NotBurnable);
            }

            let mut wallet = self.wallet_mut(target);

            if wallet.free_balance < amount {
                return Err(Error::InsufficientFunds);
            }

            wallet.free_balance = wallet
                .free_balance
                .checked_sub(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            *self.supply = self
                .supply
                .checked_sub(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            Ok(())
        }

        fn withdraw_free(&mut self, amount: u128) -> Result<(), Error> {
            self.try_unlock(self.env().caller())?;
            let source = self.wallet_mut(self.env().caller());

            if source.free_balance < amount {
                return Err(Error::InsufficientFunds);
            }

            source.remove_free(amount)?;
            Ok(())
        }

        /// Transfer tokens from one account to another.
        ///
        /// If necessary, will check if any tokens can be unlocked at the current time, and unlock them.
        #[ink(message)]
        pub fn transfer(&mut self, destination: AccountId, amount: u128) -> Result<(), Error> {
            self.withdraw_free(amount)?;
            let destination = self.wallet_mut(destination);
            destination.add_free(amount)?;

            Ok(())
        }

        /// Transfer token from one account to another, but lock them to only be released at a specific time.
        ///
        /// If necessary, will check if any tokens can be unlocked at the current time, and unlock them.
        ///
        /// The recepieint will only be able to use the tokens after the specified timestamps have been reached.
        #[ink(message)]
        pub fn transfer_locked(
            &mut self,
            destination: AccountId,
            amounts: Vec<(u64, u128)>,
        ) -> Result<(), Error> {
            let total_amount = amounts.iter().map(|(_, amount)| amount).sum();
            self.withdraw_free(total_amount)?;

            let destination = self.wallet_mut(destination);
            for (timestamp, amount) in amounts {
                destination.add_locked(timestamp, amount)?;
            }

            Ok(())
        }

        /// Tries to unlock tokens from the target account, if any can be unlocked at the current time.
        #[ink(message)]
        pub fn try_unlock(&mut self, target: AccountId) -> Result<(), Error> {
            let timestamp = self.env().block_timestamp();
            let target = self.wallet_mut(target);
            target.unlock_at(timestamp)?;

            Ok(())
        }
    }

    #[cfg(test)]
    mod tests {
        use super::super::*;
        use super::*;

        use ink_env::{test::advance_block, DefaultEnvironment};
        use ink_lang as ink;

        const DEFAULT_CALLEE_HASH: [u8; 32] = [0x07; 32];
        const DEFAULT_ENDOWMENT: Balance = 1_000_000;
        const DEFAULT_GAS_LIMIT: Balance = 1_000_000;

        fn default_accounts() -> ink_env::test::DefaultAccounts<ink_env::DefaultEnvironment> {
            ink_env::test::default_accounts::<ink_env::DefaultEnvironment>().unwrap()
        }

        fn set_next_caller(caller: AccountId) {
            ink_env::test::push_execution_context::<ink_env::DefaultEnvironment>(
                caller,
                AccountId::from(DEFAULT_CALLEE_HASH),
                DEFAULT_GAS_LIMIT,
                DEFAULT_ENDOWMENT,
                ink_env::test::CallData::new(ink_env::call::Selector::new([0x00; 4])),
            )
        }

        #[ink::test]
        fn happy_path() {
            let accounts = default_accounts();

            set_next_caller(accounts.alice);
            let mut contract = Token::new_mintable(InstantiationConfig {
                name: "Test Token".to_string(),
                tag: "TTKN".to_string(),
                decimals: 6,
                burnable_by_token_authority: true,
                burnable_by_holder: true,
            });

            assert_eq!(*contract.authority, accounts.alice);
            assert_eq!(contract.info.decimals, 6);
            assert_eq!(*contract.supply, 0);

            contract
                .mint(accounts.bob, 1_000_000_000)
                .expect("should not fail");

            assert_eq!(contract.free_balance_of(accounts.bob), 1_000_000_000);

            set_next_caller(accounts.bob);

            contract
                .transfer(accounts.charlie, 5_000_000)
                .expect("should not fail");
            assert_eq!(contract.free_balance_of(accounts.charlie), 5_000_000);
            assert_eq!(
                contract.free_balance_of(accounts.bob),
                1_000_000_000 - 5_000_000
            );

            set_next_caller(accounts.alice);

            contract
                .burn(accounts.bob, 1_000_000_000 - 5_000_000)
                .expect("should not fail");

            assert_eq!(contract.free_balance_of(accounts.bob), 0);
            assert_eq!(contract.total_supply(), 5_000_000);
        }

        #[ink::test]
        fn locked() {
            let accounts = default_accounts();

            set_next_caller(accounts.alice);
            let mut contract = Token::new_mintable(InstantiationConfig {
                name: "Test Token".to_string(),
                tag: "TTKN".to_string(),
                decimals: 6,
                burnable_by_token_authority: true,
                burnable_by_holder: true,
            });

            contract
                .mint(accounts.alice, 1_000_000_000)
                .expect("should not fail");

            contract
                .transfer_locked(accounts.bob, vec![(5, 1000), (10, 10000), (15, 10000)])
                .unwrap();

            set_next_caller(accounts.bob);

            contract
                .transfer(accounts.charlie, 1000)
                .expect_err("should fail");

            advance_block::<DefaultEnvironment>().unwrap();

            contract
                .transfer(accounts.charlie, 1000)
                .expect("should succeed");

            contract
                .transfer(accounts.charlie, 20000)
                .expect_err("should fail");

            advance_block::<DefaultEnvironment>().unwrap();
            advance_block::<DefaultEnvironment>().unwrap();

            contract
                .transfer(accounts.charlie, 20000)
                .expect("should succeed");
        }
    }
}
